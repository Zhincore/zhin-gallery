import { redirect, createServerData$ } from "solid-start/server";

export default function User() {
  const data = createServerData$((_, { request }) => {
    console.log(request.url, request.method);
    throw redirect(import.meta.env.VITE_PUBLIC_HERMES + "?return_to=" + request.url);

    return { id: 1 };
  });

  return <div>User {data()?.id}</div>;
}
