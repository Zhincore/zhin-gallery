-- CreateTable
CREATE TABLE "User" (
    "name" TEXT NOT NULL,
    "password" TEXT NOT NULL,

    CONSTRAINT "User_pkey" PRIMARY KEY ("name")
);

-- CreateTable
CREATE TABLE "Category" (
    "label" TEXT NOT NULL,

    CONSTRAINT "Category_pkey" PRIMARY KEY ("label")
);

-- CreateTable
CREATE TABLE "Tag" (
    "label" TEXT NOT NULL,
    "promptKeywords" TEXT[],

    CONSTRAINT "Tag_pkey" PRIMARY KEY ("label")
);

-- CreateTable
CREATE TABLE "ImageRecord" (
    "path" TEXT NOT NULL,
    "mime" TEXT,
    "ext" TEXT,
    "metadata" JSONB,
    "isSD" BOOLEAN NOT NULL DEFAULT false,
    "sdModel" TEXT,
    "sdPrompt" TEXT,
    "hidePrompt" BOOLEAN,
    "categoryLabel" TEXT NOT NULL,

    CONSTRAINT "ImageRecord_pkey" PRIMARY KEY ("path")
);

-- CreateTable
CREATE TABLE "_ImageRecordToTag" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_ImageRecordToTag_AB_unique" ON "_ImageRecordToTag"("A", "B");

-- CreateIndex
CREATE INDEX "_ImageRecordToTag_B_index" ON "_ImageRecordToTag"("B");

-- AddForeignKey
ALTER TABLE "ImageRecord" ADD CONSTRAINT "ImageRecord_categoryLabel_fkey" FOREIGN KEY ("categoryLabel") REFERENCES "Category"("label") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ImageRecordToTag" ADD CONSTRAINT "_ImageRecordToTag_A_fkey" FOREIGN KEY ("A") REFERENCES "ImageRecord"("path") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ImageRecordToTag" ADD CONSTRAINT "_ImageRecordToTag_B_fkey" FOREIGN KEY ("B") REFERENCES "Tag"("label") ON DELETE CASCADE ON UPDATE CASCADE;
